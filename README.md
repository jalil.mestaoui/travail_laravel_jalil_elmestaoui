# travail_laravel_Jalil_ELMESTAOUI




## Modification apportées 
### Dans travail_laravel_jalil_elmestaoui/laracar/app/Http/Controllers/TravelController.php


```
    public function search(Request $request) {
        $travels = $this->travelService->search(
            $request->input('departure'),
            $request->input('destination')
        );

        return view('showcase.search', ['travels' => $travels]);
    }

    public function delete($id) {

        $travel = Travel::where("id", $id)->first();
        if($travel == null) {
            return redirect()->back();
        }
        $travel->delete();
        return redirect('/travels');
    }

```
### Creation de fichier travail_laravel_jalil_elmestaoui/laracar/resources/views/showcase/search.blade.php pour l'affichage du resulat de la  recherche 

### pour tester le fonctionnement des deux fonctionnalitées : importer la base de données dans Mysql laravel.sql

